type BeatInfo = {
    bpm: number
    confidence: number
}

async function setupBeatDetection(beatCallback: (data: BeatInfo) => void): Promise<void> {
    const audioCtx = new AudioContext();
    if (!navigator.mediaDevices) {
        console.error("browser unable to access media devices");
        return;
    }
    const stream = await navigator.mediaDevices.getUserMedia({ "audio": true });
    const microphone = audioCtx.createMediaStreamSource(stream);

    const context = microphone.context;

    context.audioWorklet.addModule(new URL("./worklets/beat-detector-worklet.min.js", import.meta.url)).then(() => {
        const node = new AudioWorkletNode(context, "beat-detector-processor");
        console.log(context.sampleRate);
        node.port.onmessage = event => {
            beatCallback(event.data);
        };

        microphone.connect(node).connect(context.destination);
    });
}

setupBeatDetection((data) => {
    console.log(data);
});