import aubio from "aubiojs";

class BeatDetectorProcessor extends AudioWorkletProcessor {
    constructor() {
        super();
        aubio().then(({ Tempo }) => {
            this.tempo = new Tempo(128 * 4, 128, sampleRate);
        });
    }

    onBeat(err, bpm) {
        console.log(err, bpm);
    }

    process(inputs) {
        if (inputs[0] === undefined)
            return true;

        //const input = inputs[0];

        if (this.tempo === undefined)
            return true;


        if (this.tempo.do(inputs[0][0])) {
            this.port.postMessage({ bpm: this.tempo.getBpm(), confidence: this.tempo.getConfidence() });
            //console.log("bpm", this.tempo.getBpm(), "confidence", this.tempo.getConfidence(), Math.random());
        }

        return true;
    }
}

registerProcessor("beat-detector-processor", BeatDetectorProcessor);