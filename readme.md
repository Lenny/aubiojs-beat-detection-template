# aubiojs-beat-detection-template
Small starter project that uses [aubiojs](https://github.com/qiuxiang/aubiojs) in an [Audio Worklet](https://developer.mozilla.org/en-US/docs/Web/API/AudioWorklet) to do beat detection on an audio input device. 

## Usage
1. Before doing anything do `npm run build`. This will build the Audio Worklet bundles. (Right now webpack does not handle imports in worklets directly yet - so we have to pre bundle it).
2. Run `npm start` to start a development server that auto reloads on code changes. See the console for the beat detection outputs
    - *Note*: if you change the audio worklet itself, you will need to stop the dev server and go back to step 1.
3. Run `npm run build` to build a production ready bundle.